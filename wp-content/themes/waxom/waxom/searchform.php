<form class="search-form relative" id="search-form" action="<?php echo esc_url(home_url('/')); ?>/">
	<input name="s" id="s" type="text" value="" placeholder="<?php esc_html_e('Search...','waxom') ?>" class="search">
	<button class="search-button"><i class="fa fa-search"></i></button>
</form>					