<?php 

// Load theme widgets

include_once( get_template_directory() . '/framework/widgets/lib/widget-flickr.php');
include_once( get_template_directory() . '/framework/widgets/lib/widget-contact-details.php');
include_once( get_template_directory() . '/framework/widgets/lib/widget-blog-posts.php');
include_once( get_template_directory() . '/framework/widgets/lib/widget-social-icons.php');